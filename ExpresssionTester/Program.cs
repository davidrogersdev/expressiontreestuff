﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LicenceTracker.Entities;
using LicenceTracker.Entities.Db;

namespace ExpresssionTester
{
    class Program
    {
        class MyClass
        {
            public int id { get; set; }
        }
        static void Main(string[] args)
        {
            Database.SetInitializer(new LicenceTrackerInitializer());

            var l = new LicenceTrackerContext();
            l.Configuration.LazyLoadingEnabled = false;
            l.Configuration.ProxyCreationEnabled = false;
            //l.Database.Initialize(true);

            IExpressionBuilder expressionBuilder = new ExpressionBuilder();

            var filters = new[]
            {
                new Filter { Operation = Op.LessThan, PropertyName = "Id", Value = 4 },
                new Filter { Operation = Op.EndsWith, PropertyName = "Name", Value = "OS" }
            };

            Expression<Func<Software, bool>> idAndNameFilterExp = expressionBuilder.MakePredicateAnd<Software>(filters);

            var query = l.SoftwareProducts.Where(idAndNameFilterExp);

            var selector = expressionBuilder.MakeSelector<Software, string>("Name");
            var orderByDesc = expressionBuilder.GetExpressionSort<Software, string>("Description");

            var newQueryable = query.OrderByDescending(orderByDesc)
                .Select(selector);

            newQueryable.ToList().ForEach(Console.WriteLine);

            Console.ReadLine();
        }
    }
}
