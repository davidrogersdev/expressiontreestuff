﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Linq.Expressions;
using System.ComponentModel;

namespace ExpresssionTester
{
    public interface IExpressionBuilder
    {
        Expression<Func<T, TResult>> MakeSelector<T, TResult>(string path);
        Expression<Func<T, TResult>> GetExpressionSort<T, TResult>(string propertyToOrderBy);

        Func<IQueryable<T>, IOrderedQueryable<T>> GetExpressionSortFunc<T, TResult>(
            string propertyToOrderBy,
            ListSortDirection listSortDirection);

        Expression<Func<T, bool>> MakePredicateAnd<T>(IEnumerable<Filter> filters);
        Expression<Func<T, bool>> MakePredicateOr<T>(IEnumerable<Filter> filters);
    }
}
