﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LicenceTracker.Entities.Mapping
{
    public class LicenceAllocationMap : EntityTypeConfiguration<LicenceAllocation>
    {
        public LicenceAllocationMap()
        {
            // Primary Key
            HasKey(l => l.Id);

            // Properties
            Property(l => l.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(l => l.PersonId).IsRequired();
            Property(l => l.LicenceId).IsRequired();
            Property(l => l.StartDate).IsRequired();

            // Relationships
            HasRequired(u => u.Licence)
                 .WithMany()
                 .HasForeignKey(u => u.LicenceId)
                 .WillCascadeOnDelete(true);

            HasRequired(u => u.Person)
                 .WithMany()
                 .HasForeignKey(u => u.PersonId)
                 .WillCascadeOnDelete(true);

        }
    }
}
