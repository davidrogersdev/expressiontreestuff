﻿using System.Collections.Generic;

namespace LicenceTracker.Entities
{
    public class Person
    {
        public Person()
        {
            LicenceAllocations = new List<LicenceAllocation>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<LicenceAllocation> LicenceAllocations { get; set; }
    }
}
