﻿using System.Collections.Generic;

namespace LicenceTracker.Entities
{
    public class SoftwareType
    {
        public SoftwareType()
        {
            Softwares = new List<Software>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Software> Softwares { get; set; }
    }
}
