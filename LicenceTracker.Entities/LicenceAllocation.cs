﻿using System;

namespace LicenceTracker.Entities
{
    public class LicenceAllocation
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int LicenceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public virtual Licence Licence { get; set; }
        public virtual Person Person { get; set; }
    }
}
