﻿using System.Collections.Generic;

namespace LicenceTracker.Entities
{
    public class Software
    {
        public Software()
        {
            Licences = new List<Licence>();
            SoftwareFiles = new List<SoftwareFile>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int SoftwareTypeId { get; set; }
        public virtual ICollection<Licence> Licences { get; set; }
        public virtual ICollection<SoftwareFile> SoftwareFiles { get; set; }
        public virtual SoftwareType SoftwareType { get; set; }
    }
}
