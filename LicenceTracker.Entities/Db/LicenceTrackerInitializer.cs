﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using LicenceTracker.Entities.Enums;

namespace LicenceTracker.Entities.Db
{
    //public class LicenceTrackerInitializer : DropCreateDatabaseAlways<LicenceTrackerContext>
    public class LicenceTrackerInitializer : DropCreateDatabaseIfModelChanges<LicenceTrackerContext>
    //public class LicenceTrackerInitializer : CreateDatabaseIfNotExists<LicenceTrackerContext>
    {
        protected override void Seed(LicenceTrackerContext context)
        {
            var people = new List<Person>
            {
                new Person {FirstName = "Terry", LastName = "Halpin"},
                new Person {FirstName = "Alan", LastName = "Turing"},
                new Person {FirstName = "Edgar", LastName = "Codd"},
                new Person {FirstName = "William", LastName = "Gates"},
                new Person {FirstName = "Steven", LastName = "Jobs"},
                new Person {FirstName = "Larry", LastName = "Page"},
                new Person {FirstName = "Sergey", LastName = "Brin"},
                new Person {FirstName = "Brendan", LastName = "Eich"},
                new Person {FirstName = "Yukihiro", LastName = "Matsumoto"},
                new Person {FirstName = "Guido", LastName = "van Rossum"}
            };

            context.People.AddRange(people); context.SaveChanges();

            var softwareTypes = new List<SoftwareType>
            {
                new SoftwareType {Description = "Desktop Operating System", Name = "DesktopOS"},
                new SoftwareType {Description = "Server Operating System", Name = "ServerOS"},
                new SoftwareType {Description = "Table or Phone Operating System", Name = "TabletOS"}
            };

            context.SoftwareTypes.AddRange(softwareTypes); context.SaveChanges();
            //context.SaveChanges();

            var softwareProducts = new List<Software>
            {
                new Software {Name = "Windows 7", Description = "Very popular", SoftwareTypeId = 1},
                new Software {Name = "OpenSuse", Description = "Very awesome", SoftwareTypeId = 2},
                new Software {Name = "CentOS", Description = "Stable Server", SoftwareTypeId = 2},
                new Software {Name = "Android", Description = "Massively popular", SoftwareTypeId = 3},
                new Software {Name = "IOS", Description = "Bad workflow", SoftwareTypeId = 3},
                new Software {Name = "Symbian", Description = "Abandoned by Nokia", SoftwareTypeId = 3},
                new Software {Name = "Maemo", Description = "Another great one gone", SoftwareTypeId = 3},
                new Software {Name = "Tizen", Description = "Samsung's effort for a phone/tablet OS", SoftwareTypeId = 3}
            };

            context.SoftwareProducts.AddRange(softwareProducts); context.SaveChanges();

            var softwareFiles = new List<SoftwareFile>
            {
                new SoftwareFile {FileType = FileTypes.exe, FileName = "Win7.exe", SoftwareId = 1},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "Win7SP1.iso", SoftwareId = 1},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "OpenSuseDvd.iso", SoftwareId = 2},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "OpenSuseDvdExtra.iso", SoftwareId = 2},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "CentOS.iso", SoftwareId = 3},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "AndroidKitkat.iso", SoftwareId = 4},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "IOS.iso", SoftwareId = 5},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "Symbian.iso", SoftwareId = 6},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "Maemo.iso", SoftwareId = 7},
                new SoftwareFile {FileType = FileTypes.iso, FileName = "Tizen.iso", SoftwareId = 8}
            };

            context.SoftwareFiles.AddRange(softwareFiles); context.SaveChanges();

            var licences = new List<Licence>
            {
                new Licence
                {
                    LicenceKey = "ECDAE080-2419-4EF9-B551-299A1B8A7019",
                    SoftwareId = 1
                },
                new Licence
                {
                    LicenceKey = "A717BFE6-3886-4E64-BD15-CBC3211A0C56",
                    SoftwareId = 2
                },
                new Licence
                {
                    LicenceKey = "626F7EFA-E3AA-45A7-8314-065AE30809BF",
                    SoftwareId = 1
                },
                new Licence
                {
                    LicenceKey = "6452BA47-EF33-4FF1-918F-AC14D9E4828B",
                    SoftwareId = 3
                },
                new Licence
                {
                    LicenceKey = "CB0861DE-5CF4-4AE7-A235-D604357EA0CC",
                    SoftwareId = 4
                },
                new Licence
                {
                    LicenceKey = "CCDD1C7A-249E-4A11-B295-162545A9BD86",
                    SoftwareId = 5
                },
                new Licence
                {
                    LicenceKey = "93E300B0-77C9-4E5D-9163-6CD9F792E50F",
                    SoftwareId = 6
                },
                new Licence
                {
                    LicenceKey = "58F9EAD6-2AAF-4CF8-BF95-510BDE69E4EF",
                    SoftwareId = 7
                },
                new Licence
                {
                    LicenceKey = "886B9CCE-B229-4E1F-B701-EDC938DB354F",
                    SoftwareId = 8
                }
            };

            context.Licences.AddRange(licences); context.SaveChanges();
            //context.SaveChanges();

            var licenceAllocations = new List<LicenceAllocation>
            {
                new LicenceAllocation
                {
                    EndDate = new DateTime(2011, 4, 3),
                    PersonId = 1,
                    StartDate = new DateTime(2010, 4, 4),
                    LicenceId = 1
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2011, 4, 3),
                    PersonId = 3,
                    StartDate = new DateTime(2010, 4, 4),
                    LicenceId = 2
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2011, 4, 3),
                    PersonId = 3,
                    StartDate = new DateTime(2010, 4, 4),
                    LicenceId = 3
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2012, 4, 3),
                    PersonId = 2,
                    StartDate = new DateTime(2011, 4, 4),
                    LicenceId = 3
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2012, 4, 3),
                    PersonId = 6,
                    StartDate = new DateTime(2011, 4, 4),
                    LicenceId = 1
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2012, 4, 3),
                    PersonId = 8,
                    StartDate = new DateTime(2011, 4, 4),
                    LicenceId = 2
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2013, 4, 3),
                    PersonId = 2,
                    StartDate = new DateTime(2012, 4, 4),
                    LicenceId = 1
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2014, 4, 3),
                    PersonId = 2,
                    StartDate = new DateTime(2013, 4, 4),
                    LicenceId = 1
                },
                new LicenceAllocation
                {
                    EndDate = new DateTime(2013, 4, 3),
                    PersonId = 2,
                    StartDate = new DateTime(2012, 4, 4),
                    LicenceId = 3
                },
                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 2,
                    StartDate = new DateTime(2013, 4, 4),
                    LicenceId = 3
                },
                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 6,
                    StartDate = new DateTime(2012, 4, 4),
                    LicenceId = 2
                },
                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 6,
                    StartDate = new DateTime(2013, 4, 4),
                    LicenceId = 2
                },                
                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 8,
                    StartDate = new DateTime(2010, 2, 27),
                    LicenceId = 5
                },
                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 6,
                    StartDate = new DateTime(2011, 3, 14),
                    LicenceId = 6
                },                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 3,
                    StartDate = new DateTime(2012, 8, 18),
                    LicenceId = 7
                },                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 7,
                    StartDate = new DateTime(2009, 12, 25),
                    LicenceId = 8
                },                new LicenceAllocation
                {
                    EndDate = null,
                    PersonId = 7,
                    StartDate = new DateTime(2011, 11, 30),
                    LicenceId = 9
                }
            };

            context.LicenceAllocations.AddRange(licenceAllocations); context.SaveChanges();

            base.Seed(context);
        }
    }
}
