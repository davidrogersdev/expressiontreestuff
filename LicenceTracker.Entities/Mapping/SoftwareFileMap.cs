﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LicenceTracker.Entities.Mapping
{
    public class SoftwareFileMap : EntityTypeConfiguration<SoftwareFile>
    {
        public SoftwareFileMap()
        {
            // Primary Key
            HasKey(sf => sf.Id);

            // Properties
            Property(sf => sf.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(sf => sf.FileName).IsRequired().IsVariableLength().HasMaxLength(250);
            Property(sf => sf.FileType).IsRequired();

            // Relationships
            HasRequired(u => u.Software)
                 .WithMany()
                 .HasForeignKey(u => u.SoftwareId)
                 .WillCascadeOnDelete(true);
        }
    }
}
