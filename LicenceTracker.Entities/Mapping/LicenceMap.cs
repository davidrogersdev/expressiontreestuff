﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LicenceTracker.Entities.Mapping
{
    public class LicenceMap : EntityTypeConfiguration<Licence>
    {
        public LicenceMap()
        {
            // Primary Key
            HasKey(l => l.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(l => l.LicenceKey).HasMaxLength(250).IsRequired().IsVariableLength();
            Property(l => l.SoftwareId).IsRequired();

            // Relationships
            this.HasRequired(u => u.Software)
                 .WithMany()
                 .HasForeignKey(u => u.SoftwareId)
                 .WillCascadeOnDelete(true);
        }
    }
}
