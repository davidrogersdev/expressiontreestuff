﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LicenceTracker.Entities.Mapping
{
    public class PersonMap : EntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            // Primary Key
            HasKey(p => p.Id);

            // Properties
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.FirstName).IsRequired().IsVariableLength().HasMaxLength(20);
            Property(p => p.LastName).IsRequired().IsVariableLength().HasMaxLength(20);

            // Relationships
            /* Dealt with by entity on other side of relationship */

        }
    }
}
